package com.info.country;

import com.info.country.model.CountryInfo;
import java.util.ArrayList;
/**
 * Interface which will define the contract for CountryInfoListActivity
 */
interface CountryInfoListContract {
    interface Presenter extends BasePresenter {
        void onRefreshButtonClick();
        void requestDataFromServer();
    }

    interface View extends BaseView<Presenter> {
        void showProgress();
        void hideProgress();
        void setDataToRecyclerView(ArrayList<CountryInfo> countryInfoList, String mainTitle);
        void onResponseFailure(Throwable throwable);
    }

    interface GetCountryInfoIntractor {
        interface OnFinishedListener {
             void onFinished(ArrayList<CountryInfo> countryInfoList,String mainTitle);
            void onFailure(Throwable t);
        }
        void getCountryInfoList(OnFinishedListener onFinishedListener);
        void getCountryInfoFromDatabase(OnFinishedListener onFinishedListener);
    }
}

