package com.info.country.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.info.country.R;
import com.info.country.model.CountryInfo;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
/**
 * Adapter class for the recycler view
 */
public class CountryInfoListAdapter extends RecyclerView.Adapter<CountryInfoListAdapter.CountryInfoViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(CountryInfo countryInfo,View view);
        void onViewClick(CountryInfo countryInfo,View view);
    }
    private ArrayList<CountryInfo> dataList;
    private Context mContext;
    public OnItemClickListener listener;

    public CountryInfoListAdapter(Context context, ArrayList<CountryInfo> dataList,OnItemClickListener listener) {
        this.dataList = dataList;
        mContext=context;
        this.listener=listener;
    }

    @Override
    public CountryInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.country_info_list_row, parent, false);
        return new CountryInfoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountryInfoViewHolder holder, int position) {
        holder.txtCountryInfoTitle.setText(dataList.get(position).getTitle());
        holder.txtCountryInfoDescription.setText(dataList.get(position).getDescription());
        //picaso library for lazy loading and caching of the image.
        Picasso
                .with(mContext)
                .load(dataList.get(position).getImageHref())
                .fit()
                .into((ImageView) holder.imageView);

        holder.bind(dataList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class CountryInfoViewHolder extends RecyclerView.ViewHolder {
        TextView txtCountryInfoTitle, txtCountryInfoDescription;
        ImageView imageView;
        CountryInfoViewHolder(View itemView) {
            super(itemView);
            txtCountryInfoTitle =  itemView.findViewById(R.id.country_info_title);
            txtCountryInfoDescription =  itemView.findViewById(R.id.country_info_description);
            imageView =  itemView.findViewById(R.id.image_view);

        }

        public void bind(final CountryInfo item, final OnItemClickListener listener) {
            //listener for the list item in the recycler view
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    listener.onItemClick(item,view);
                }
            });
            //listener for the imageview in the list item
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    listener.onViewClick(item,view);
                }
            });
        }

    }


}
