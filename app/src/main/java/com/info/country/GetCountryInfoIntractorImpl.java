package com.info.country;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.info.country.dao.CountryInfoDatabase;
import com.info.country.model.CountryInfo;
import com.info.country.model.CountryInfoDatabaseModel;
import com.info.country.model.CountryInfoList;
import com.info.country.network.RetrofitInstance;
import com.info.country.service.GetCountryInfoDataService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.content.Context.MODE_PRIVATE;
/**
 * Interactor class which will directly interact with backend server and database
 * and supply data to presenter class
 */
public class GetCountryInfoIntractorImpl implements CountryInfoListContract.GetCountryInfoIntractor {

    private Context mContext;
    private static final String SHARED_PREFS_NAME="country_info_prefs";
    public GetCountryInfoIntractorImpl(Context context) {
        mContext=context;
    }
    /**
     * Get the country information from backend server via retrofit framework
     */
    @Override
    public void getCountryInfoList(final OnFinishedListener onFinishedListener) {
        /** Create handle for the RetrofitInstance interface*/
        GetCountryInfoDataService service = RetrofitInstance.getRetrofitInstance().create(GetCountryInfoDataService.class);

        /** Call the method with parameter in the interface to get  data*/
        Call<CountryInfoList> call = service.getCountryInfoData();
        call.enqueue(new Callback<CountryInfoList>() {
            @Override
            public void onResponse(Call<CountryInfoList> call, Response<CountryInfoList> response) {
                Log.wtf("SUCCESS", response.body().getCountryInfoArrayList().toString());
                onFinishedListener.onFinished(response.body().getCountryInfoArrayList(),response.body().getCountryInfoMainTitle());
                mapWithCountryInfoDatabaseModel(response.body().getCountryInfoArrayList());
                saveMainTitleToSharedPrefs(response.body().getCountryInfoMainTitle());
            }

            @Override
            public void onFailure(Call<CountryInfoList> call, Throwable t) {
                Log.wtf("FAILURE",  "");
                onFinishedListener.onFailure(t);

            }
        });

    }
    /**
     * Get the country information from room database
     */
    public void getCountryInfoFromDatabase(final OnFinishedListener onFinishedListener){
        CountryInfoDatabase database = CountryInfoDatabase.getAppDatabase(mContext);
        List<CountryInfoDatabaseModel> listofCountryInfo=database.countryInfoDao().getAll();
        ArrayList<CountryInfo> listOfCountryInfo=mapWithCountryInfoModel(listofCountryInfo);
        String mainTitle=getMainTitleFromSharedPrefs();
        onFinishedListener.onFinished(listOfCountryInfo,mainTitle);
    }
    /**
     * Map the data coming from network response to database model class to persist the
     * data to room database
     */
    private void mapWithCountryInfoDatabaseModel(ArrayList<CountryInfo> countryinfos) {
        CountryInfo countryInfo;
        if (countryinfos != null) {
            ArrayList<CountryInfoDatabaseModel> countryInfoDatabaseModels=new ArrayList<CountryInfoDatabaseModel>();
            Iterator<CountryInfo> iter = countryinfos.iterator();

            while (iter.hasNext()) {
                countryInfo = iter.next();
                if (countryInfo.getTitle()!=null && countryInfo.getTitle()!="") {
                    CountryInfoDatabaseModel countryInfoDatabaseModel = new CountryInfoDatabaseModel();
                    countryInfoDatabaseModel.setTitle(countryInfo.getTitle());
                    countryInfoDatabaseModel.setDescription(countryInfo.getDescription());
                    countryInfoDatabaseModel.setImageurl(countryInfo.getImageHref());
                    countryInfoDatabaseModels.add(countryInfoDatabaseModel);
                }
            }
            persistCountryInformation(countryInfoDatabaseModels);
        }
    }
    /**
     * Map the data coming from room database to arraylist for passing to the recycler
     * adapter
     */
    private ArrayList<CountryInfo> mapWithCountryInfoModel(List<CountryInfoDatabaseModel> listofCountryInfo) {
        CountryInfoDatabaseModel countryInfodbmodel;
        ArrayList<CountryInfo> countryInfos=new ArrayList<CountryInfo>();
        if (listofCountryInfo != null) {
            Iterator<CountryInfoDatabaseModel> iter = listofCountryInfo.iterator();
            while (iter.hasNext()) {
                countryInfodbmodel=iter.next();
                CountryInfo countryInfo= new CountryInfo(countryInfodbmodel.getTitle(),countryInfodbmodel.getDescription(),countryInfodbmodel.getImageurl());
                countryInfos.add(countryInfo);
            }
        }
        return countryInfos;
    }
    //saving the data to database
    private void persistCountryInformation(ArrayList<CountryInfoDatabaseModel> listOfCountryInfos) {
        CountryInfoDatabase database = CountryInfoDatabase.getAppDatabase(mContext);
        database.countryInfoDao().insertAllCountryInfos(listOfCountryInfos);
    }

    private void saveMainTitleToSharedPrefs(String mainTitle){
        SharedPreferences.Editor editor = mContext.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("mainTitle", mainTitle);
        editor.apply();
    }

    private String getMainTitleFromSharedPrefs(){
        SharedPreferences prefs = mContext.getSharedPreferences(SHARED_PREFS_NAME, MODE_PRIVATE);
        String mainTitle = prefs.getString("mainTitle", "No name defined");
        return mainTitle;
    }
}

