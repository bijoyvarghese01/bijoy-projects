package com.info.country;
/**
 * Base class for all the presenter
 */
interface BasePresenter {
    public void onCreate();
    public void onResume();
    public void onPause();
    public void onDestroy();
}
