package com.info.country.service;

import com.info.country.model.CountryInfoList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetCountryInfoDataService {
    @GET("facts.json")
    Call<CountryInfoList> getCountryInfoData();
}
