package com.info.country;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.info.country.model.CountryInfo;
import java.util.ArrayList;
/**
 * Presenter class which with act as bridge b/w UI and model and handles the business
 * logic here.Mostly this handles the data request from server/sqlite
 */
public class CountryInfoListPresenter implements CountryInfoListContract.Presenter,CountryInfoListContract.GetCountryInfoIntractor.OnFinishedListener{

    private CountryInfoListContract.View view;
    private CountryInfoListContract.GetCountryInfoIntractor getCountryInfoIntractor;
    private Context mContext;
    public CountryInfoListPresenter(CountryInfoListContract.View view, CountryInfoListContract.GetCountryInfoIntractor getCountryInfoIntractor,Context context) {
        this.view = view;
        this.getCountryInfoIntractor = getCountryInfoIntractor;
        this.mContext=context;
    }
    @Override
    public void onCreate() {
    }
    /**
     * Not used as of now .Reserving for future button clicks
     */
    @Override
    public void onRefreshButtonClick() {
        if(view != null){
            view.showProgress();
        }
        getCountryInfoIntractor.getCountryInfoList(this);
    }
    /**
     * Request data from server if network connection is available , else fetch data
     * from sqlite
     */
    @Override
    public void requestDataFromServer() {
        if(checkNetworkConnectionAvailable()) {
            getCountryInfoIntractor.getCountryInfoList(this);
            }
            else {
            getCountryInfoIntractor.getCountryInfoFromDatabase(this);
        }
    }

    private boolean checkNetworkConnectionAvailable(){
        ConnectivityManager ConnectionManager=(ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true ) {
            return true;
        }
        else{
            return false;
        }

    }

    @Override
    public void onFinished(ArrayList<CountryInfo> countryInfoArrayList,String mainTitle) {

        if(view != null){
            view.setDataToRecyclerView(countryInfoArrayList,mainTitle);
            view.hideProgress();

        }
    }

    @Override
    public void onFailure(Throwable t) {

        if(view != null){
            view.onResponseFailure(t);
            view.hideProgress();

        }

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {
       view=null;
    }
}
