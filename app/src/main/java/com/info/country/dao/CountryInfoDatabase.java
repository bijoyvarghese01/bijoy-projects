package com.info.country.dao;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import com.info.country.model.CountryInfoDatabaseModel;
/**
 * Dao class for room database
 */
@Database(entities = {CountryInfoDatabaseModel.class}, version = 1)
public abstract class CountryInfoDatabase extends RoomDatabase {
    private static CountryInfoDatabase INSTANCE;
    public abstract CountryInfoDao countryInfoDao();
    public static CountryInfoDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), CountryInfoDatabase.class, "info-database")
                            // allow queries on the main thread
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }
    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

}