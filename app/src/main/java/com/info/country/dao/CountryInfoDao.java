package com.info.country.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import com.info.country.model.CountryInfoDatabaseModel;
import java.util.List;
/**
 * Dao class for room database
 */
    @Dao
    public interface CountryInfoDao {

        @Query("SELECT * FROM countryinfo")
        List<CountryInfoDatabaseModel> getAll();

        @Query("SELECT COUNT(*) from countryinfo")
        int countCountryInfos();

        @Insert
        void insert(CountryInfoDatabaseModel countryInfo);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAllCountryInfos(List<CountryInfoDatabaseModel> countryinfos);

        @Delete
        void delete(CountryInfoDatabaseModel countryInfo);

    }
