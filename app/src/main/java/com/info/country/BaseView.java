package com.info.country;
/**
 * Base class for all the views
 */
interface BaseView<T> {
    public void setPresenter(T presenter);
}
