package com.info.country.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * Model class to parse the rows as arraylist.This also parse the main title
 */
public class CountryInfoList {

    @SerializedName("rows")
    private ArrayList<CountryInfo> countryInfoList;

    @SerializedName("title")
    private String mainTitle;

    public ArrayList<CountryInfo> getCountryInfoArrayList() {
        return countryInfoList;
    }

    public void setCountryInfoArrayList(ArrayList<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;

    }

    public String getCountryInfoMainTitle() {
        return mainTitle;
    }

    public void setCountryInfoMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;

    }
}