package com.info.country.model;

import android.support.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
/**
 * Model class for the retrofit library for parsing the response
 */
public class CountryInfo {

    @NonNull
    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("imageHref")
    private String imageHref;

    public CountryInfo(String title, String description, String imageHref) {
        this.title = title;
        this.description = description;
        this.imageHref = imageHref;

    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }


    public void setdescription(String description) {
        this.description = description;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }

}
