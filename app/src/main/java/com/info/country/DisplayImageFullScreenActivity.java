package com.info.country;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
/**
 * Activity class which will display the Image in a full screen
 */
public class DisplayImageFullScreenActivity extends AppCompatActivity {
    /**
     * Loading the image in photo view via Picasso library.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("View Image");
        setContentView(R.layout.activity_display_image_full_screen);
        PhotoView photoView = findViewById(R.id.imageViewFullScreen);
        Picasso
                .with(this)
                .load(getIntent().getStringExtra("imageUrl"))
                .fit()
                .into( photoView);
    }
}
