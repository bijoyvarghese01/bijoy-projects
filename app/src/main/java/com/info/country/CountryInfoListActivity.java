package com.info.country;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.info.country.adapter.CountryInfoListAdapter;
import com.info.country.model.CountryInfo;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
/**
 * Activity class which will display the information in a recycler view.
 */
public class CountryInfoListActivity extends AppCompatActivity implements CountryInfoListContract.View{

    private  CountryInfoListContract.Presenter presenter;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    private Context mContext;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext=this;
        initializeToolbarAndRecyclerView();
        initProgressBar();
        presenter = new CountryInfoListPresenter(this, new GetCountryInfoIntractorImpl(this),this);
        presenter.requestDataFromServer();
    }

    /**
     * Initializing Toolbar and RecyclerView
     */
    private void initializeToolbarAndRecyclerView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recycler_view_country_info_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CountryInfoListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.simpleSwipeRefreshLayout);
        setOnRefreshListenerForListView();
    }

    /**
     * Initializing progressbar
     */
    private void initProgressBar() {
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);
        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);
        this.addContentView(relativeLayout, params);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);

    }
    /**
     * Pull to refresh functionality added for recycler view
     */
    public void setOnRefreshListenerForListView() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                presenter.requestDataFromServer();
            }
        });
    }

    /**
     * Populate the recycler view with a list of information either from
     * network or from local database
     */
    @Override
    public void setDataToRecyclerView(ArrayList<CountryInfo> countryInfoArrayList,String mainTitle) {
        CountryInfoListAdapter adapter = new CountryInfoListAdapter(this,countryInfoArrayList,new CountryInfoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CountryInfo item, View view) {

            }
            @Override
            public void onViewClick(CountryInfo item, View view) {

                    Intent intent = new Intent(mContext, DisplayImageFullScreenActivity.class);
                    intent.putExtra("imageUrl", item.getImageHref());
                    startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
        if(mainTitle!=null) {
            toolbar.setTitle(mainTitle);
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(CountryInfoListActivity.this,
                "Something went wrong...Error message: " + throwable.getMessage(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(CountryInfoListContract.Presenter presenter) {
    }
}
