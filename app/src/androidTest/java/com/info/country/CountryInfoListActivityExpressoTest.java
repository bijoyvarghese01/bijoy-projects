package com.info.country;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.click;

@RunWith(AndroidJUnit4.class)
public class CountryInfoListActivityExpressoTest {

    @Rule
    public ActivityTestRule<CountryInfoListActivity> mActivityRule =
            new ActivityTestRule<>(CountryInfoListActivity.class);

    @Test
    public void ensureRecyclerViewIsDisplayed() {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view_country_info_list))
                .check(ViewAssertions.matches((ViewMatchers.isDisplayed())));


    }

    @Test
    public void checkRecyclerViewClick() {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view_country_info_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }

    }

